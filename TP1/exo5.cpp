#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot
    if (n < 1) {
       return 0;
    }
    n--;
    float t = z.x;

    z.x = (z.x)*(z.x)-(z.y)*(z.y) + point.x;
    z.y = 2*t*z.y + point.y;

    if(z.length() < 2){
       return isMandelbrot(z, n, point);
    }else{
      return 1;
    }

    isMandelbrot(z, n, point);


    // check n

    // check length of z
    // if Mandelbrot, return 1 or n (check the difference)
    // otherwise, process the square of z and recall
    // isMandebrot
    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



