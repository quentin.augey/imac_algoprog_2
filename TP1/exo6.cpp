#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    // your code
    int length;
    int capacite;
};


void initialise(Liste* liste)
{
    (*liste).premier = nullptr;
}

bool est_vide(const Liste* liste)
{
    if((*liste).premier == nullptr){
        return true;
    }
    return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud *noeud = new Noeud;
    (*noeud).donnee = valeur;
    (*noeud).suivant = nullptr;

    Noeud* save = new Noeud;
    save = liste->premier;

    if(est_vide(liste)){
      cout << "ajout first" <<endl;
      (*liste).premier = (noeud);
      return;
    }

    while(save->suivant != nullptr){
       save = save->suivant;
    }
    save->suivant = noeud;
}

void affiche(const Liste* liste)
{
    Noeud* save = new Noeud;
    save = liste->premier;

    if(est_vide(liste)){
        cout << "vide" <<endl;
        return;
    }

    while(save->suivant != nullptr){
       cout << "Data : " << save->donnee << endl;
       save = save->suivant;
    }

}

int recupere(const Liste* liste, int n)
{

    Noeud* save = new Noeud;
    save = liste->premier;

    for(int i = 0; i < n; i++){
       save = save->suivant;
    }

    return  save->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    int pos=1;
    Noeud* save = new Noeud;
    save = liste->premier;

    while(save->donnee != valeur){
       save = save->suivant;
       pos++;
    }

    return pos;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud* save = new Noeud;
    save = liste->premier;

    for(int i = 0; i < n-1; i++){
       save = save->suivant;
    }
    save->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{

    if(tableau->length<tableau->capacite){

         tableau->donnees[tableau->length] = valeur;
         tableau->length++;

     }else{

         tableau->capacite++;
         int* stock =(int*)realloc(tableau->donnees,tableau->capacite*sizeof(int));

         if(stock==NULL){

             cout << "memory error" <<endl;

         }
         else{

             tableau->donnees=stock;
             tableau->donnees[tableau->length]= valeur;
             tableau->length++;

         }
     }


}


void initialise(DynaTableau* tableau, int capacite)
{

    tableau->length=0;

    tableau->capacite=capacite;

    tableau->donnees=(int*)malloc(sizeof(int)*capacite);

}

bool est_vide(const DynaTableau* liste)
{

    return liste->length==0;

}

void affiche(const DynaTableau* tableau)
{
    for(int i=0; i<tableau->length;i++){

        cout << tableau->donnees[i] << endl;

    }

}

int recupere(const DynaTableau* tableau, int n)
{
    if(n>tableau->length){

        cout<< "Pas assez de donnée dans le tableau" <<endl;
        return 0;
    }

    return tableau->donnees[n];

}

int cherche(const DynaTableau* tableau, int valeur)
{
    for(int i=0; i<tableau->length;i++){

        if(tableau->donnees[i]==valeur){

            return i;

        }
    }

    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    //j'avais mis == au lieu de =
    tableau->donnees[n]=valeur;

}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste,valeur);

}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{

    Noeud* provisoire = liste->premier;

    liste->premier=liste->premier->suivant;

    return provisoire->donnee;

}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{

    ajoute(liste,valeur);

}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    if (liste->premier != nullptr){

        Noeud* first = liste->premier;
        Noeud* save;

        if (first->suivant != nullptr){

            save = first->suivant;

            while (save->suivant != nullptr){

                first = first->suivant;
                save = first->suivant;

            }

            first->suivant = nullptr;
            return save->donnee;

        }else{

            liste->premier = nullptr;
            return first->donnee;

        }
    }

}



int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
