#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
    Array& sorted=w->newArray(toSort.size());
    sorted[0]=toSort[0];
    int m=sorted[0];
    int taille=1;
    for(int i=1; i<toSort.size();i++){
        bool inserted = false;
        for(int j=0;j<taille;j++){
            if(sorted[j]>toSort[i]){
                sorted.insert(j,toSort[i]);
                inserted = true;
                break;
            }
        }
    if(inserted ==false){
    sorted.insert(taille,toSort[i]);
}

    taille+=1;
    }
    toSort=sorted;


}
    // insertion sort from toSort to sorted


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
    w->show();

    return a.exec();
}
