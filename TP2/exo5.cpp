#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if(origin.size() <= 1){
        return;
    }
	// initialisation
	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    int count = 0;

    for(count; count < first.size(); count++){
        first[count] = origin[count];
    }

    for(int i = 0 ; i < second.size(); i++){
        second[i] = origin[count];
        count++;
    }


	// recursiv splitAndMerge of lowerArray and greaterArray

    splitAndMerge(first);
    splitAndMerge(second);
	// merge

    merge(first, second, origin);

}

void merge(Array& first, Array& second, Array& result)
{

    int indiceFirst = 0;
    int indiceSecond = 0;
    int i = 0;

    while(indiceFirst < first.size() && indiceSecond < second.size()){
        if(first[indiceFirst]<second[indiceSecond]){
            result[i] = first[indiceFirst];
            indiceFirst++;

        }else{
            result[i] = second[indiceSecond];
            indiceSecond++;

        }
        i++;
    }

    for(int j = indiceFirst; j<first.size(); j++){
       result[i] = first[j];
       i++;
    }
    for(int k = indiceSecond; k<second.size(); k++){
       result[i] = second[k];
       i++;
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
