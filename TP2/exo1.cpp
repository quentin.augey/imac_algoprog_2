#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){

    // selectionSort

    for (int indice=0; indice<toSort.size(); indice++)
    {
        for (int min=0; min<toSort.size(); min++)
        {
            if (toSort[min] > toSort[indice])
                toSort.swap(indice, min);
        }
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
