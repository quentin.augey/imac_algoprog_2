#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	
    if(size <= 1){
        return;
    }

	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);



    int lowerSize = 0, greaterSize = 0; // effectives sizes

    int pivot = toSort[0];
    int countLower = 0;
    int countGreater = 0;

    for(int i = 1; i < size; i++){
        if(toSort[i] <= pivot){
            lowerArray[countLower] = toSort[i];
            countLower++;
        }
    }

    for(int i = 1; i < size; i++){
        if(toSort[i] > pivot){
            greaterArray[countGreater] = toSort[i];
            countGreater++;
        }
    }

	// split
	// recursiv sort of lowerArray and greaterArray
    recursivQuickSort(lowerArray, countLower);
    recursivQuickSort(greaterArray, countGreater);

    int countMerge = 0;


    for(int i = 0; i < countLower; i++){
     toSort[i] = lowerArray[i];
     countMerge++;
    }

    toSort[countMerge] = pivot;
     countMerge++;

    for(int i = countMerge; i <= countLower + countGreater; i++){
     toSort[i] = greaterArray[i-countMerge];
    }
	// merge

}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
