#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{

   return nodeIndex*2+1;

}

int Heap::rightChild(int nodeIndex)
{

   return nodeIndex*2+2;;

}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i

	int i = heapSize;

    (*this)[i] = value;

    while(i > 0 && (*this)[i] > (*this)[(i-1)/2]){
        this->swap(i, (i-1)/2);
        i = (i-1)/2;
    }

}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i

	int i_max = nodeIndex;
    int largest = nodeIndex;

    if(this->leftChild(nodeIndex) < heapSize && (*this)[this->leftChild(nodeIndex)]>(*this)[largest]){

        largest = this->leftChild(nodeIndex);

    }

    if(this->rightChild(nodeIndex) < heapSize && (*this)[this->rightChild(nodeIndex)]>(*this)[largest]){

        largest = this->rightChild(nodeIndex);

    }

    if(largest != i_max){

        this->swap(i_max, largest);
        heapify(heapSize, largest);

    }

}

void Heap::buildHeap(Array& numbers)
{



}

void Heap::heapSort()
{

   int heapSize = this->size();

   for(int i = heapSize-1; i > 0; i--){
       this->swap(0, i);
       this->heapify(i, 0);
   }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
