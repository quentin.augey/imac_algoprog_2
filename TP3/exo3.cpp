#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        this->value = value;
        this->left = nullptr;
        this->right = nullptr;
        // init initial node without children
    }

	void insertNumber(int value) {

        if(value > this->value) {
            if(this->right==nullptr){
                this->right = new SearchTreeNode(value);
            }else{
               this->right->insertNumber(value);
            }
        }else{
            if(this->left==nullptr){
                this->left = new SearchTreeNode(value);
            }else{
                this->left->insertNumber(value);
            }
        }
        // create a new node and insert it in right or left child
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        SearchTreeNode *node = new SearchTreeNode(value);

        int countLeft = 0;
        int countRight = 0;

        if(this->left == nullptr){
            countLeft = 0;
        }else{
            countLeft = this->left->height();
        }

        if(this->right == nullptr){
            countRight = 0;
        }else{
            countRight = this->right->height();

        }

        if(countRight >= countLeft){
            return countRight+1;
        }else{
            return countLeft+1;
        }

    }

    uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        if (this->left == nullptr && this->right==nullptr){
           return 1;
       }

       if( this->left == nullptr){
           return this->right->nodesCount()+1;

       }
       if( this->right == nullptr){
           return this->left->nodesCount()+1;

       }

       return this->left->nodesCount()+this->right->nodesCount()+1;

   }




	bool isLeaf() const {

        if(this->left == nullptr && this->right==nullptr){
            return true;
        }else{

        // return True if the node is a leaf (it has no children)
        return false;
        }
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {

        // fill leaves array with all leaves of this tree

        if (this->isLeaf()){

            leaves[leavesCount] = this;
            leavesCount++;
        }

        if (this->right != nullptr){

            this->right->allLeaves(leaves, leavesCount);

        }

        if (this->left != nullptr){

            this->left->allLeaves(leaves, leavesCount);

        }

	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel

        if (this->left != nullptr){

            this->left->inorderTravel(nodes, nodesCount);

        }

        nodes[nodesCount] = this;
        nodesCount++;

        if (this->right != nullptr){

            this->right->inorderTravel(nodes, nodesCount);

        }

	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel

        nodes[nodesCount] = this;
        nodesCount++;

        if (this->left != nullptr){

            this->left->preorderTravel(nodes, nodesCount);

        }

        if (this->right != nullptr){

            this->right->preorderTravel(nodes, nodesCount);

        }

	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel

        if (this->left != nullptr){

            this->left->postorderTravel(nodes, nodesCount);

        }

        if (this->right != nullptr){

            this->right->postorderTravel(nodes, nodesCount);

        }

        nodes[nodesCount] = this;
        nodesCount++;

	}

	Node* find(int value) {
        // find the node containing value

        //on regarde la value actuelle et on retourne direct si c'est la bonne
        if (this->value == value){

            return this;


        } else {

    //sinon on regarde si la value actuelle est inferieur si elle l'est on regarde à droite (toutes les values supérieurs à this) sinon on regarde à gauche (toutes les values inferieurs à this)

            if (this->value < value ){

                if (this->right != nullptr){
                       //on relance la fonction jusqu'a retourner la valeur recherchée
                    return this->right->find(value);

                }



            } else if (this->left != nullptr){

                return this->left->find(value);

            }
        }

        // find the node containing value
        return nullptr;


		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
