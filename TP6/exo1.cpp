#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
    /**
      * Make a graph from a matrix
      * first create all nodes, add it to the graph then connect them
      * this->appendNewNode
      * this->nodes[i]->appendNewEdge
      */

    for(int i = 0; i < nodeCount; i++){

        GraphNode* graphnoeud = new  GraphNode(i);
        this->appendNewNode(graphnoeud);

    }

    for(int j = 0; j < nodeCount; j++){

        for(int k = 0; k<nodeCount; k++){

            if(adjacencies[j][k] != 0){

                this->nodes[j]->appendNewEdge(nodes[k],adjacencies[j][k]);

            }

        }

    }

}

void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
	  */


    visited[(*first).value] = true;

    for(Edge* edge = (*first).edges; edge != NULL; edge = edge->next){

        if(!visited[edge->destination->value]){

            nodes[nodesSize]=edge->destination;
            nodesSize++;
            deepTravel(edge->destination, nodes , nodesSize, visited);


        }

    }

}

void Graph::wideTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	 * Fill nodes array by travelling graph starting from first and using queue
	 * nodeQueue.push(a_node)
	 * nodeQueue.front() -> first node of the queue
	 * nodeQueue.pop() -> remove first node of the queue
	 * nodeQueue.size() -> size of the queue
	 */

    std::queue<GraphNode*> nodeQueue;
    nodeQueue.push(first);

    //nodeQueue.empty() != NULL
    while (!nodeQueue.empty()) {

        GraphNode* visitedNode = nodeQueue.front();

        nodeQueue.pop();
        visited[visitedNode->value] = 1;
        nodes[nodesSize++] = visitedNode;

        for(Edge* edge = visitedNode->edges; edge != NULL; edge = edge->next){

            if(!visited[edge->destination->value]){
                nodeQueue.push(edge->destination);
            }

        }

    }

}

bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/

    std::queue<GraphNode*> nodeQueue;
    nodeQueue.push(first);

    //nodeQueue.empty() != NULL
    while (!nodeQueue.empty()) {

        GraphNode* visitedNode = nodeQueue.front();

        nodeQueue.pop();
        visited[visitedNode->value] = 1;

        for(Edge* edge = visitedNode->edges; edge != NULL; edge = edge->next){

            if(!visited[edge->destination->value]){

                nodeQueue.push(edge->destination);

            }else{

                return true;

            }
        }
    }

    return false;

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
